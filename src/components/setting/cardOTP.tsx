import { useState } from 'react';
import ConfirmOTP from './confirmOTP';
import { useMutation } from 'react-query';
import { useTranslation } from 'next-i18next';
import { useMe } from '@/data/user';

function CardOTP() {
  const { t } = useTranslation('common');
  const [isOTPModalVisible, setIsOTPModalVisible] = useState(false);
  const { me } = useMe();
  return (
    <div className="mt-10">
      <h2 className="underline text-xl">Đăng ký nhận Card OTP</h2>
      <div className="text-center flex">
        <p className=" mt-7 text-xl">Đăng ký Online :</p>
      </div>
      <div className="text-center">
        {me && me?.otp_card === null && (
          <button
            className="border w-96 text-center mt-11 bg-green-500 p-4 text-white rounded text-lg"
            onClick={() => {
              setIsOTPModalVisible(true);
            }}
          >
            Đăng ký
          </button>
        )}
        {me && me?.otp_card && me?.otp_card.status === 'active' && (
          <button
            className="border w-96 text-center mt-11 bg-gray-300 p-4 text-white rounded text-lg"
            disabled
          >
            Đã Đăng Ký
          </button>
        )}
      </div>

      <div className="text-center flex">
        {me && me?.otp_card && me?.otp_card.status === 'active' && (
          <p className=" mt-7 text-xl">Đăng ký tại văn phòng :</p>
        )}
      </div>
      <div className="text-center ">
        <p className=" mt-10 text-lg">
          Bạn vui lòng đến văn phòng của Tomiru để đăng ký và nhận thẻ cứng tại
          :
        </p>
        <p className="text-lg">
          Tầng 3 Toà Nhà HUD3-TOWER 123 Đ. Tô Hiệu, P. Nguyễn Trãi, Hà Đông, Hà
          Nội, Việt Nam
        </p>
      </div>

      {isOTPModalVisible && <ConfirmOTP showOTP={setIsOTPModalVisible} />}
    </div>
  );
}
export default CardOTP;
