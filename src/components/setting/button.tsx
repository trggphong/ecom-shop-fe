function Button(props: any) {
  const stylesOn = 'bg-green-500 p-4 text-white rounded text-sm';
  const stylesOff = 'hover:bg-gray-100 p-4 rounded ';
  return (
    <div className="mt-9">
      <button
        className={`${props.action === 'methodOTP' ? stylesOn : stylesOff}`}
        onClick={() => {
          props.settingAction('methodOTP');
        }}
      >
        Cấu hình OTP
      </button>
      <button
        className={`ml-5 ${props.action === 'cardOTP' ? stylesOn : stylesOff}`}
        onClick={() => {
          props.settingAction('cardOTP');
        }}
      >
        Đăng ký Card OTP
      </button>
    </div>
  );
}
export default Button;
