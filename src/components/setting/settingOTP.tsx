import { useState, useEffect } from 'react';
import Link from 'next/link';
import routes from '@/config/routes';
import Button from '../ui/button';
import { useTranslation } from 'next-i18next';
import { useMutation, useQueryClient } from 'react-query';
import client from '@/data/client';
import toast from 'react-hot-toast';
import { useMe } from '@/data/user';
import { API_ENDPOINTS } from '@/data/client/endpoints';
function SettingOTP(props: any) {
  const [selectedOTPType, setSelectedOTPType] = useState<any>();
  const { t } = useTranslation('common');
  const { me } = useMe();

  const queryClient = useQueryClient();
  const handleSelectedOption = (event: any) => {
    setSelectedOTPType(event.target.value);
  };

  const [err, setErr] = useState<any>();
  const sendErr = (input: String) => {
    setErr(input);
    setTimeout(() => {
      setErr('');
    }, 5000);
  };
  const { mutate, isLoading } = useMutation(client.users.update, {
    onSuccess: () => {
      toast.success(<b>{t('text-profile-page-success-toast')}</b>, {
        className: '-mt-10 xs:mt-0',
      });
    },
    onError: (error) => {
      sendErr('cập nhập không thành công');
      toast.error(<b>{t('text-profile-page-error-toast')}</b>, {
        className: '-mt-10 xs:mt-0',
      });
      console.log(error);
    },
    onSettled: () => {
      queryClient.invalidateQueries(API_ENDPOINTS.USERS_ME);
    },
  });
  function saveOTPSetting() {
    if (!selectedOTPType) {
      return sendErr('Có lỗi xảy ra vui lòng thử lại');
    }
    if (me && selectedOTPType === 'card' && !me.otp_card) {
      return sendErr('Bạn chưa đăng ký card OTP, xin hãy đăng ký');
    }
    if (me && selectedOTPType === 'sms' && !me.profile.contact) {
      return sendErr('Bạn chưa thiết lập số điện thoại');
    }
    if (me && me.default_otp_type === selectedOTPType) {
      return toast.success(<b>{t('text-profile-page-success-toast')}</b>, {
        className: '-mt-10 xs:mt-0',
      });
    }

    if (me && me.default_otp_type) {
      const sendData = {
        id: me.id,
        name: me.name,
        profile: me.profile,
        default_otp_type: selectedOTPType,
      };
      mutate(sendData);
    }
  }
  useEffect(() => {
    if (me) {
      setSelectedOTPType(me.default_otp_type.toString());
    }
  }, []);
  if (!me) {
    return sendErr('Vui lòng đăng nhập để thực hiện thay đổi.');
  }
  return (
    <>
      <h2 className="mt-12 text-lg underline">
        Chọn phương thức nhận OTP mặc định
      </h2>
      <div className="flex mt-12">
        <div className="w-1/3 text-2xl">
          <select
            id="selectOption"
            name="selectOption"
            value={selectedOTPType}
            onChange={handleSelectedOption}
            className="rounded w-21"
          >
            <option value="" disabled>
              -- Chọn --
            </option>
            <option value="email">Email</option>
            <option value="sms">SMS</option>
            <option value="card">Card</option>
          </select>
        </div>
        <div className="mt-4">
          {selectedOTPType === 'email' && (
            <div className="text-center">
              <h3 className="text-3xl w-full">Email nhận mã OTP của bạn là:</h3>
              <p className="text-2xl mt-8 text-green-700">{me?.email}</p>
            </div>
          )}
          {selectedOTPType === 'sms' && (
            <div className=" text-center">
              {me.profile.contact && (
                <>
                  <h3 className="text-3xl  ">
                    Số điện thoại nhận mã OTP của bạn là:
                  </h3>
                  <p className="text-2xl mt-8 text-green-700">
                    +{me.profile.contact}
                  </p>
                </>
              )}

              {!me.profile.contact && (
                <>
                  <h3 className="text-3xl  ">Bạn chưa cài số điện thoại</h3>
                  <div className="mt-8 text-xl text-blue-500 underline">
                    <Link href={routes.profile}>Cài đặt ngay </Link>
                  </div>
                </>
              )}
            </div>
          )}
          {selectedOTPType === 'card' && (
            <div className=" text-center">
              {me && !me.otp_card && (
                <>
                  <h3 className="text-3xl  ">
                    Bạn chưa đăng ký thẻ xin hãy đăng ký
                  </h3>
                  <div
                    className="mt-8 text-xl text-blue-500 underline"
                    onClick={() => {
                      props.settingAction('cardOTP');
                    }}
                  >
                    <Link href="#">Đăng ký thẻ ngay </Link>
                  </div>
                </>
              )}
              {me && me?.otp_card && me?.otp_card.status === 'active' && (
                <>
                  <h3 className="text-3xl  ">Số serial thẻ của bạn là :</h3>
                  <p className="text-2xl mt-8 text-green-700">
                    {me.otp_card.card_serial}
                  </p>
                </>
              )}
            </div>
          )}
        </div>
      </div>
      {err && <p className="text-center text-red-500 mt-20">{err}</p>}
      <div className="mt-auto flex items-center gap-4 pb-3 lg:justify-end">
        <Button
          className="flex-1 sm:flex-none"
          isLoading={isLoading}
          disabled={isLoading}
          onClick={saveOTPSetting}
        >
          {t('text-save-changes')}
        </Button>
      </div>
    </>
  );
}

export default SettingOTP;
