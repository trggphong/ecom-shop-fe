import { useMutation, useQueryClient } from 'react-query';
import { useTranslation } from 'next-i18next';
import Button from '@/components/ui/button';
import client from '@/data/client';
import toast from 'react-hot-toast';
import { useRouter } from 'next/router';
import { useRef, useState, useEffect } from 'react';
import CryptData from '@/lib/hooks/use-crypt-data';
import { useMe } from '@/data/user';
import useCountdownTimer from '@/lib/hooks/use-CountdownTimer';
import { API_ENDPOINTS } from '@/data/client/endpoints';
const ConfirmOTP = (props: any) => {
  const { me } = useMe();
  const { t } = useTranslation('common');
  const inputRef = useRef<any>(null);
  const token = '458875' + me?.id + me?.email;
  const [err, setErr] = useState<any>();
  const queryClient = useQueryClient();
  const [isCreatingPassword, setIsCreatingPassword] = useState(false);
  const {
    minutes,
    remainingSeconds,
    startCountdown,
    isActive,
    setIsActive,
    seconds,
    setSeconds,
  } = useCountdownTimer();

  const { encryptData, data: aesData, key, iv } = CryptData();
  //create secret_token
  useEffect(() => {
    encryptData(token, key, iv);
  }, [token]);
  // This useEffect is used to calculate the countdown timer.(OTP)
  useEffect(() => {
    if (isActive) {
      startCountdown();
    }
  }, [isActive]);

  function offTable() {
    props.showOTP(false);
  }
  //API create card OTP
  const { mutate: createCard, isLoading: loadingCreate } = useMutation(
    client.cardOTP.createCard,
    {
      onSuccess: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.USERS_ME);
        toast.success(<b>Đăng ký thành công</b>);
        props.showOTP(false);
      },
      onError: (error: any) => {
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );
  //API verify otp
  const { mutate: verifyOTP, isLoading: loadingVerify } = useMutation(
    client.cardOTP.verifyOTP,
    {
      onSuccess: () => {
        setIsCreatingPassword(true);
        inputRef.current.value = '';
        setIsActive(false);
      },
      onError: (error: any) => {
        // sendErr('Mã OTP của bạn không chính xác');
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );
  function confirmHandler(e: any) {
    e.preventDefault();
    if (!inputRef.current.value) {
      return sendErr('Vui lòng không để trống thông tin');
    }
    if (!isCreatingPassword) {
      const sendData = {
        user_id: me?.id,
        type_otp: 'create_otp',
        otp: inputRef.current.value,
      };
      verifyOTP(sendData);
    }
    if (isCreatingPassword) {
      const sendData = {
        user_id: me?.id,
        password: inputRef.current.value,
      };
      createCard(sendData);
    }
  }

  const { mutate: sendOTP, isLoading: ld } = useMutation(
    client.cardOTP.getOTP,
    {
      onSuccess: (res) => {
        console.log('check res', res);
        setIsActive(true);
        setSeconds(120);
      },
      onError: (error: any) => {
        sendErr('Yêu cầu gửi OTP không thành công');
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );
  function verify() {
    if (me?.id && me?.email) {
      const sendData = {
        user_id: me?.id,
        user_email: me?.email,
        type_otp: 'create_otp',
        secret_token: aesData,
        method: 'email',
      };
      sendOTP(sendData);
    }
  }

  function refresh(e: any) {
    e.preventDefault();
    if (seconds === 0) {
      verify();
    }
  }
  const sendErr = (input: string) => {
    setErr(input);
    setTimeout(() => {
      setErr('');
    }, 5000);
  };
  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 top-0 bg-black bg-opacity-50 ">
      <div className="relative w-96 h-auto mx-auto max-w-screen-sm flex-col p-6 pt-6 sm:p-5 sm:pt-8 md:pt-10 3xl:pt-12 bg-light shadow-card dark:bg-dark-250 dark:shadow-none text-center flex items-center justify-center rounded ">
        <button onClick={offTable} className="absolute top-0 right-0 p-4 ">
          X
        </button>
        <div className="mb-8 leading-tight">
          {!isCreatingPassword && (
            <>
              <h2 className="text-center  text-lg mb-10">
                Xác nhận đăng ký Card
              </h2>
              <p>Mã xác thực của bạn được gửi về</p>
              <p>{me?.email}</p>
            </>
          )}
          {isCreatingPassword && (
            <>
              <h2 className="text-center  text-lg mb-10">
                Tạo mật khẩu cho thẻ của bạn
              </h2>
              <p>chúng tôi sẽ gửi file thẻ mềm cho bạn</p>
              <p>vui lòng tạo mật khẩu cho file</p>
            </>
          )}

          <div>
            <input
              className="rounded mt-5"
              ref={inputRef}
              placeholder={
                isCreatingPassword ? 'Thêm mật khẩu' : 'Nhập mã xác thực'
              }
            ></input>
            {isActive && (
              <p className="mt-5 text-red-400">
                {minutes}:{remainingSeconds < 10 ? '0' : ''}
                {remainingSeconds}
              </p>
            )}
            {ld && (
              <p className="text-red-400 mt-5">Mã OTP của bạn đang được gửi</p>
            )}
            {loadingVerify && (
              <p className="text-red-400 mt-5">Vui lòng chờ trong giây lát</p>
            )}
          </div>
          <div>
            {!isCreatingPassword && (
              <button
                className={`mt-5 border  p-1 rounded-lg  ${
                  isActive
                    ? 'pointer-events-none '
                    : 'border-green-500 bg-green-600 text-white'
                }`}
                onClick={refresh}
              >
                Gửi mã
              </button>
            )}
            {isCreatingPassword && (
              <p className="mt-5">
                bạn muốn nhận thẻ cứng, hãy đến văn phòng của chúng tôi
              </p>
            )}
            {err && <p className="text-red-500">{err}</p>}
          </div>
        </div>

        <div className="flex">
          <Button
            className="mr-10 "
            onClick={confirmHandler}
            isLoading={isCreatingPassword ? loadingCreate : loadingVerify}
          >
            {t('text-submit-confirm')}
          </Button>
          <Button className="bg-red-500 hover:bg-red-300" onClick={offTable}>
            {t('text-cancel')}
          </Button>
        </div>
      </div>
    </div>
  );
};
export default ConfirmOTP;
