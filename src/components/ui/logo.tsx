import cn from 'classnames';
import Image from '@/components/ui/image';
import AnchorLink from '@/components/ui/links/anchor-link';
import routes from '@/config/routes';
import { useIsMounted } from '@/lib/hooks/use-is-mounted';
import { useIsDarkMode } from '@/lib/hooks/use-is-dark-mode';
import { siteSettings } from '@/data/static/site-settings';
import { useSettings } from '@/data/settings';
import NextLink from 'next/link';

export default function Logo({
  className = 'w-20',
  ...props
}: React.AnchorHTMLAttributes<{}>) {
  const isMounted = useIsMounted();
  const { isDarkMode } = useIsDarkMode();
  const { lightLogo, darkLogo } = siteSettings;
  const { settings }: any = useSettings();
  return (
    <>
      <AnchorLink
        href={routes.home}
        className={cn(
          'relative flex items-center text-dark focus:outline-none dark:text-light',
          className,
        )}
        {...props}
      >
        <span
          className="relative overflow-hidden"
          style={{
            width: siteSettings?.width,
            height: siteSettings?.height,
          }}
        >
          {isMounted && isDarkMode && (
            <div className="flex ml-5 w-14">
              <Image
                // src={settings?.dark_logo?.original ?? darkLogo}
                src="/icons/logo.png"
                loading="eager"
                alt={settings?.siteTitle ?? 'Dark Logo'}
                className="object-contain"
                priority
                width={39}
                height={50}
              />
            </div>
          )}
          {isMounted && !isDarkMode && (
            <div className="flex ml-5 w-14">
              <Image
                // src={settings?.dark_logo?.original ?? darkLogo}
                src="/icons/logo.png"
                loading="eager"
                alt={settings?.siteTitle ?? 'Dark Logo'}
                className="object-contain"
                priority
                width={39}
                height={50}
              />
            </div>
          )}
        </span>
      </AnchorLink>
      <NextLink href={routes.home}>
        <div className="">
          {isMounted && !isDarkMode && (
            <h2 className="text-center text-yellow-300 text-2xl m-0">TOMIRU</h2>
          )}
          {isMounted && isDarkMode && (
            <h2 className="text-center text-white text-2xl m-0">TOMIRU</h2>
          )}
        </div>
      </NextLink>
    </>
  );
}
