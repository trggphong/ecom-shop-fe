import { useMutation } from 'react-query';
import { useTranslation } from 'next-i18next';
import Button from '@/components/ui/button';
import client from '@/data/client';
import { useCart } from '@/components/cart/lib/cart.context';
import toast from 'react-hot-toast';
import { useRouter } from 'next/router';
import { useRef, useState, useEffect } from 'react';
import ThankYou from '@/pages/orders/[tracking_number]/thank-you';
import CryptData from '@/lib/hooks/use-crypt-data';
import { useMe } from '@/data/user';
import useCountdownTimer from '@/lib/hooks/use-CountdownTimer';

const OrderOTP = (props: any) => {
  const { me } = useMe();
  const { t } = useTranslation('common');
  const OTPref = useRef<any>(null);
  const [thank, setThank] = useState(false);
  const token = '458875' + props.order.customer.id + props.order.customer.email;
  const [selectedOption, setSelectedOption] = useState<any>();
  const [randomCard, setRandomCard] = useState<any>();
  const {
    minutes,
    remainingSeconds,
    startCountdown,
    isActive,
    setIsActive,
    seconds,
    setSeconds,
  } = useCountdownTimer();
  const handleSelectedOption = (event: any) => {
    setSelectedOption(event.target.value);
  };

  const { encryptData, data: encryptedToken, key, iv } = CryptData();
  useEffect(() => {
    encryptData(token, key, iv);
  }, [token]);

  useEffect(() => {
    if (isActive) {
      startCountdown();
    }
  }, [isActive]);

  const { mutate, isLoading, data } = useMutation(client.payment.post, {
    onSuccess: (res) => {
      toast.success(<b>{t('payment-success')}</b>);
      setThank(true);
      setIsActive(false);
      setTimeout(() => {
        offTable();
      }, 15000);
    },

    onError: (error: any) => {
      OTPref.current.value = '';
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });

  function offTable() {
    props.showOTP(false);
    setThank(false);
  }

  function paymentHandler(e: any) {
    e.preventDefault();
    if (OTPref.current.value.length === 0) {
      sendError('vui lòng không để trống thông tin');
      return toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    }

    const sendData = {
      from_id: props.order.customer.id,
      from_user_email: props.order.customer.email,
      total_tomxu: props.order.total_tomxu,
      customer_contact: props.order.customer_contact,
      otp: OTPref.current.value,
      type_otp: 'verify_order',
      tracking_number: props.order.tracking_number,
      secret_token: encryptedToken,
      products: props.order.products.map((item: any) => ({
        product_id: item.id,
        quantity: Number(item.pivot.order_quantity),
        tomxu: item.tomxu.price_tomxu,
        tomxu_subtotal: item.tomxu.price_tomxu * item.pivot.order_quantity,
        shop_id: item.shop_id,
      })),
    };

    mutate(sendData);
  }

  const { mutate: sendOTP, isLoading: ld } = useMutation(
    client.payment.sendOTP,
    {
      onSuccess: () => {
        setIsActive(true);
        setSeconds(120);
      },
      onError: (error: any) => {
        sendError('Yêu cầu gửi OTP của bạn không thành công');
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );

  function verify() {
    if (selectedOption) {
      const sendData = {
        user_id: props.order.customer.id,
        user_email: props.order.customer.email,
        type_otp: 'verify_order',
        total_tomxu: props.order.total_tomxu,
        secret_token: encryptedToken,
        method: selectedOption,
      };
      sendOTP(sendData);
    }
  }

  function OTPHandler(e: any) {
    e.preventDefault();
    if (seconds === 0 && selectedOption === 'card') {
      setIsActive(true);
      setSeconds(120);
      return randomNumber();
    }
    if (selectedOption !== 'card' && seconds === 0) {
      verify();
    }
  }
  const randomNumber = () => {
    setRandomCard(Math.floor(Math.random() * 35) + 1);
    setIsActive(true);
    setSeconds(120);
  };
  useEffect(() => {
    if (seconds === 0 && selectedOption === 'card') {
      randomNumber();
    }
  }, [seconds]);
  useEffect(() => {
    if (me) {
      setSelectedOption(me.default_otp_type.toString());
    }
  }, []);

  const [err, setErr] = useState<string>();
  function sendError(message: string) {
    setErr(message);
    setTimeout(() => {
      setErr('');
    }, 5000);
  }

  const CheckPoint = () => {
    return (
      <div className="w-1/3 mt-6">
        {!isActive && (
          <select
            id="selectOption"
            name="selectOption"
            value={selectedOption}
            onChange={handleSelectedOption}
            className="rounded w-21"
          >
            <option value="" disabled>
              -- Chọn --
            </option>
            <option value="email">Email</option>
            <option value="sms">SMS</option>
            <option value="card">Card</option>
          </select>
        )}
        {isActive && (
          <select
            id="selectOption"
            name="selectOption"
            value={selectedOption}
            onChange={handleSelectedOption}
            className="rounded w-21"
            disabled
          >
            <option value="" disabled>
              -- Chọn --
            </option>
            <option value="email">Email</option>
            <option value="sms">SMS</option>
            <option value="card">Card</option>
          </select>
        )}
      </div>
    );
  };

  const Instruction = () => {
    // return (
    //   <div className="w-auto h-80 flex">
    //     <div className="w-96"></div>
    //     <div>
    //       <h3 className="border-t-2 w-96 text-lg">
    //         Hướng dẫn lấy OTP trên Card
    //       </h3>
    //     </div>
    //     <div className="w-96"></div>
    //   </div>
    // );
    return <></>;
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 top-0 bg-black bg-opacity-50">
      {!thank && (
        <div className="relative w-auto h-auto mx-auto max-w-screen-sm flex-col p-6 pt-6 sm:p-5 sm:pt-8 md:pt-10 3xl:pt-12 bg-light shadow-card dark:bg-dark-250 dark:shadow-none text-center flex items-center justify-center rounded ">
          <button onClick={offTable} className="absolute top-0 right-0 p-4 ">
            X
          </button>
          <div className="w-full border-b-2 border-gray-300">
            <h2 className="text-center  text-xl mb-6">Vui lòng nhập OTP</h2>
          </div>
          <p className=" text-xl mt-3">Chọn phương thức gửi OTP</p>
          <div className="">
            <div className="flex mt-3 items-center justify-center">
              <CheckPoint />

              <div className="ml-2">
                {selectedOption === 'email' && (
                  <div className="ml-8 mt-5">
                    <p>Mã OTP của bạn được gửi về</p>
                    <p>{props.email}</p>
                  </div>
                )}

                {selectedOption === 'sms' && (
                  <div className="ml-8 mt-5">
                    <p>Mã OTP của bạn được gửi về</p>
                    <p>số điện thoại : {props.order.customer_contact}</p>
                  </div>
                )}
                {selectedOption === 'card' && (
                  <div className={isActive ? 'mt-5 mr-20' : 'ml-8 mt-5'}>
                    <p>Số thứ tự OTP trên card của bạn là :</p>
                    {isActive && (
                      <p className="text-gray-950 text-xl">[ {randomCard} ]</p>
                    )}
                    {!isActive && (
                      <p className="text-gray-950 text-sm">Xin hãy lấy số</p>
                    )}
                  </div>
                )}
              </div>
            </div>
            {isActive && (
              <p className="mt-5 text-red-400">
                {minutes}:{remainingSeconds < 10 ? '0' : ''}
                {remainingSeconds}
              </p>
            )}
            {ld && (
              <p className="text-red-400 mt-5">Mã OTP của bạn đang được gửi</p>
            )}
            <button
              className={`mt-7 w-20 h-8 border  p-1 rounded-lg  ${
                isActive
                  ? 'pointer-events-none '
                  : 'border-green-500 bg-green-600 text-white'
              }`}
              onClick={OTPHandler}
            >
              {selectedOption === 'card' ? 'Lấy số' : 'Gửi mã'}
            </button>
            {/* form submit otp */}
            <form
              className="border-gray-300 pb-9 mt-6"
              onSubmit={paymentHandler}
            >
              <div className="flex items-center justify-center">
                <div className="w-12"></div>
                <input
                  className="rounded mt-5 w-96 shadow-md"
                  ref={OTPref}
                  placeholder="nhập OTP"
                ></input>
                <div className="w-12"></div>
              </div>
              {err && <p className="mt-4 text-red-500">{err}</p>}
              <div className="flex items-center justify-center mt-9">
                <Button
                  className="mr-10 "
                  onClick={paymentHandler}
                  isLoading={isLoading}
                >
                  {t('text-submit-confirm')}
                </Button>
                <Button
                  className="bg-red-500 hover:bg-red-300"
                  onClick={offTable}
                >
                  {t('text-cancel')}
                </Button>
              </div>
            </form>
            {selectedOption === 'card' && isActive && <Instruction />}
          </div>
        </div>
      )}
      {thank && <div onClick={offTable}>{<ThankYou />}</div>}
    </div>
  );
};
export default OrderOTP;
