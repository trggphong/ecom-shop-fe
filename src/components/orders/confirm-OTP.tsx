import { useMutation } from 'react-query';
import { useTranslation } from 'next-i18next';
import Button from '@/components/ui/button';
import client from '@/data/client';
import { useCart } from '@/components/cart/lib/cart.context';
import toast from 'react-hot-toast';
import { useRouter } from 'next/router';
import { useRef, useState, useEffect } from 'react';
import { useOrderPayment, useOrder } from '@/data/order';
import ThankYou from '@/pages/orders/[tracking_number]/thank-you';
import CryptData from '@/lib/hooks/use-crypt-data';

const ConfirmOTP = (props: any) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const OPTRef = useRef<any>(null);
  const [seconds, setSeconds] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [thank, setThank] = useState(false);
  const token = '458875' + props.order.customer.id + props.order.customer.email;

  const { encryptData, data: aesData, key, iv } = CryptData();
  useEffect(() => {
    encryptData(token, key, iv);
  }, [token]);

  useEffect(() => {
    let intervalId: any;
    if (isActive) {
      intervalId = setInterval(() => {
        setSeconds((prevSeconds) => {
          if (prevSeconds === 0) {
            clearInterval(intervalId);
            setIsActive(false);
            return 0;
          }
          return prevSeconds - 1;
        });
      }, 1000);
    }
    return () => clearInterval(intervalId);
  }, [isActive]);
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;

  const { mutate, isLoading, data } = useMutation(client.payment.post, {
    onSuccess: (res) => {
      console.log('check', res);

      toast.success(<b>{t('payment-success')}</b>);
      setThank(true);
      setIsActive(false);
      setTimeout(() => {
        offTable();
      }, 15000);
    },

    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });

  function offTable() {
    props.showOTP(false);
    setThank(false);
  }

  function paymentHandler(e: any) {
    e.preventDefault();
    if (OPTRef.current.value.length === 0) {
      return toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    }

    const sendData = {
      from_id: props.order.customer.id,
      from_user_email: props.order.customer.email,
      total_tomxu: props.order.total_tomxu,
      customer_contact: props.order.customer_contact,
      otp: OPTRef.current.value,
      type_otp: 'verify_order',
      tracking_number: props.order.tracking_number,
      secret_token: aesData,
      products: props.order.products.map((item: any) => ({
        product_id: item.id,
        product_name: item.name,
        quantity: Number(item.pivot.order_quantity),
        tomxu: item.tomxu.price_tomxu,
        tomxu_subtotal: item.tomxu.price_tomxu * item.pivot.order_quantity,
        shop_id: item.shop_id,
      })),
    };

    mutate(sendData);
  }

  const { mutate: sendOTP, isLoading: ld } = useMutation(
    client.payment.sendOTP,
    {
      onSuccess: () => {
        setIsActive(true);
        setSeconds(120);
      },
      onError: (error: any) => {
        toast.error(<b>{t('text-profile-page-error-toast')}</b>);
      },
    },
  );
  function verify() {
    const sendData = {
      user_id: props.order.customer.id,
      user_email: props.order.customer.email,
      type_otp: 'verify_order',
      total_tomxu: props.order.total_tomxu,
      secret_token: aesData,
      method: 'email',
    };
    sendOTP(sendData);
  }

  function refresh(e: any) {
    e.preventDefault();
    if (seconds === 0) {
      verify();
    }
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 top-0 bg-black bg-opacity-50 ">
      {!thank && (
        <div className="relative w-763 h-776 mx-auto flex-col p-0   sm:p-5 sm:pt-8 md:pt-10 3xl:pt-12 bg-light shadow-card dark:bg-dark-250 dark:shadow-none text-center flex items-center justify-center rounded ">
          <button
            onClick={offTable}
            className="absolute top-0 right-0 p-4 text-2xl"
          >
            X
          </button>
          <h3 className="text-2xl absolute top-8">Vui Lòng Nhập OTP</h3>
          <div className="w-600 border-b-4 h-auto border-gray-200 flex items-center justify-center absolute top-20"></div>
          <h3 className="text-2xl absolute left-10 top-24">
            Chọn phương thức gửi OTP
          </h3>
          <div className="flex absolute top-36">
            <input type="text" className="w-206 h-69 mr-6 mt-3" />
            <button className="w-117 h-8 bg-blue-600 mt-12 rounded-lg text-xl ">
              Lấy số
            </button>
            <div className="w-307 h-85 ml-6 bg-custom-yellow relative">
              <h4 className="text-xl mt-1"> Số thự tự OTP trên card</h4>
              <div className="w-259 border-b-4 h-auto border-gray-259 flex items-center justify-center absolute top-8 left-7"></div>
              <p className="text-xl mt-4">10</p>
            </div>
          </div>
          <div className="flex absolute top-72">
            <h3 className="text-2xl mr-20 mt-3">Nhập OTP</h3>
            <input className="w-472 h-14 shadow-md ml-2"></input>
          </div>
          <div className="flex mt-12">
            <button className="w-135 h-11 bg-purple-200">Xác nhận</button>
            <button className="w-117 h-11 bg-purple-200 ml-11">Huỷ bỏ</button>
          </div>
          <div className="w-700 border-b-8 h-auto border-gray-200 flex items-center justify-center absolute  bottom-72"></div>
          <h3 className="text-xl absolute  bottom-60 underline">
            Hướng dẫn lấy OTP trên Card
          </h3>
        </div>
      )}
      {thank && <div onClick={offTable}>{<ThankYou />}</div>}
    </div>
  );
};
export default ConfirmOTP;
