import { useRouter } from 'next/router';
import { useMutation } from 'react-query';
import { useAtom } from 'jotai';
import toast from 'react-hot-toast';
import client from '@/data/client';
import LoopIcon from '@mui/icons-material/Loop';
import Button from '@/components/ui/button';
import { useCart } from '@/components/cart/lib/cart.context';
import {
  calculatePaidTotal,
  calculateTotal,
  calculateTomxu,
} from '@/components/cart/lib/cart.utils';
import { usePhoneInput } from '@/components/ui/forms/phone-input';
import {
  useWalletPointsAtom,
  verifiedTokenAtom,
} from '@/components/cart/lib/checkout';
import PaymentGrid from '@/components/cart/payment/payment-grid';
import routes from '@/config/routes';
import { useTranslation } from 'next-i18next';
import { PaymentGateway } from '@/types';
import { useSettings } from '@/data/settings';
import { REVIEW_POPUP_MODAL_KEY } from '@/lib/constants';
import Cookies from 'js-cookie';
import { useEffect, useRef, useState } from 'react';
import { useMe } from '@/data/user';
import useCryptData from '@/lib/hooks/use-crypt-data';
import PhoneInput from '@/components/ui/forms/phone-input';
import useSale from '@/lib/hooks/use-sale';
export default function CartCheckout(props: any) {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { me } = useMe();
  const [userTomxu, setUserTomxu] = useState<number>();
  const { salePrice } = useSale();
  let enData: any;
  const { encryptData, data: aesData, key, iv } = useCryptData();

  useEffect(() => {
    if (me?.id && me?.email) {
      enData = '458875' + me?.id + me.email;
    }
    if (me?.id && me?.email && aesData) {
      getUserTomxu();
    }
  }, [me, aesData]);
  useEffect(() => {
    if (enData) {
      encryptData(enData, key, iv);
    }
  }, [enData]);

  const { mutate, isLoading, data } = useMutation(client.orders.create, {
    onSuccess: (res) => {
      return router.push(`${routes.orderUrl(res.tracking_number)}/payment`);
    },
    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });
  const [use_wallet_points] = useAtom(useWalletPointsAtom);
  const [token] = useAtom(verifiedTokenAtom);
  const { items, verifiedResponse } = useCart();
  const [totalTomxu, setTotalTomxu] = useState<number>();
  const addressRef = useRef<any>();
  const [err, setErr] = useState<string>();

  const available_items = items.filter(
    (item) =>
      !verifiedResponse?.unavailable_products?.includes(item.id.toString()),
  );

  const base_amount = calculateTotal(available_items);

  useEffect(() => {
    const tomxu: any = calculateTomxu(items);
    if (tomxu) {
      setTotalTomxu(tomxu);
    }
  }, [items]);

  const totalPrice = verifiedResponse
    ? calculatePaidTotal(
        {
          totalAmount: base_amount,
          tax: verifiedResponse.total_tax,
          shipping_charge: verifiedResponse.shipping_charge,
        },
        0,
      )
    : 0;

  const { phoneNumber } = usePhoneInput();

  function createOrder(e: any) {
    e.preventDefault();
    if (!addressRef.current.value) {
      sendErr('Vui lòng không để trống thông tin');
      return toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    }

    if (!totalTomxu || !userTomxu || userTomxu < totalTomxu) {
      return sendErr('số dư ví của bạn không đủ xin hãy nạp thêm');
    }
    const data = {
      amount: base_amount,
      total: totalPrice ?? 1,
      paid_total: totalPrice ?? 1,
      products: available_items.map((item) => ({
        product_id: item.id,
        order_quantity: item.quantity,
        unit_price: item.price,
        subtotal: item.price * item.quantity,
        tomxu: item.tomxu,
        tomxu_subtotal: item.tomxu * item.quantity,
      })),
      payment_gateway: PaymentGateway.CASH,
      total_tomxu: totalTomxu,
      // salePrice === 0 ? totalTomxu : Math.round(totalTomxu * salePrice),
      use_wallet_points,
      isFullWalletPayment: false,
      ...(token && { token }),
      sales_tax: verifiedResponse?.total_tax ?? 0,
      customer_contact: phoneNumber ? phoneNumber : '1',
      address: addressRef.current.value,
    };

    mutate(data);
  }

  function rechargeHandler() {
    window.open(process.env.NEXT_PUBLIC_RECHARGE_URL, '_blank');
  }
  const { mutate: getTomxu } = useMutation(client.userTomxu.getTomxu, {
    onSuccess: (res: any) => {
      setUserTomxu(res?.data.balance);
    },
    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });
  function getUserTomxu() {
    getTomxu({
      customer_id: me?.id,
      type: 1,
      secret_token: aesData,
      email: me?.email,
    });
  }

  function sendErr(message: string) {
    setErr(message);
    setTimeout(() => {
      setErr('');
    }, 5000);
  }

  return (
    <div className="mt-10 border-t border-light-400 bg-light pt-6 pb-7 dark:border-dark-400 dark:bg-dark-250 sm:bottom-0 sm:mt-12 sm:pt-8 sm:pb-9">
      <div className="mb-6 flex flex-col gap-3 text-dark dark:text-light sm:mb-7">
        <div className="flex justify-between">
          <div className="w-full text-center">
            <h2 className="flex items-center justify-between  px-5 py-4 text-sm font-medium text-dark dark:border-dark-400 dark:text-light sm:py-5 sm:px-7 md:text-base">
              {t('text-address')}
            </h2>
            <textarea
              className="ml-4 mt-5 rounded-md w-11/12 text-black border-gray-400"
              ref={addressRef}
              required
            ></textarea>
          </div>
        </div>
        <h2 className="flex items-center justify-between  px-5 py-4 text-sm font-medium text-dark dark:border-dark-400 dark:text-light sm:py-5 sm:px-7 md:text-base">
          {t('text-checkout-title')}
        </h2>
        <div className="px-5 py-4 sm:py-6 sm:px-7">
          <PhoneInput defaultValue={me?.profile?.contact} />
        </div>
        <div className="flex justify-between mt-5">
          <p>{t('text-subtotal')}</p>
          {totalTomxu && (
            <strong className="font-semibold pr-7">{`${
              salePrice === 0 ? totalTomxu : Math.round(totalTomxu * salePrice)
            } Tomxu`}</strong>
          )}
        </div>

        <div className="flex justify-between border-t border-light-400 bg-light pt-6 pb-7 dark:border-dark-400 dark:bg-dark-250 sm:bottom-0 sm:mt-12 sm:pt-8 sm:pb-9">
          <p>{t('Số dư ví hiện tại')}</p>
          <div className="flex ">
            {userTomxu && userTomxu !== 0 && (
              <strong className="font-semibold pr-7">{`${userTomxu} Tomxu`}</strong>
            )}

            <div onClick={getUserTomxu} className="hover:scale-110">
              <LoopIcon />
            </div>
          </div>
        </div>
        <div className="flex flex-col text-center text-red-400">
          {err && <p>{err}</p>}
        </div>
        {totalTomxu && userTomxu && userTomxu > totalTomxu && (
          <button
            className="w-2/6 md:h-[50px] md:text-sm  bg-green-300  rounded-md ml-auto"
            onClick={rechargeHandler}
          >
            {t('Nạp tiền')}
          </button>
        )}
        {totalTomxu && userTomxu && userTomxu < totalTomxu && (
          <button
            className="w-2/6 md:h-[50px] md:text-sm bg-[#F91111]  hover:bg-red-300  rounded-md ml-auto"
            onClick={rechargeHandler}
          >
            {t('Nạp tiền')}
          </button>
        )}
      </div>
      <Button
        disabled={isLoading}
        isLoading={isLoading}
        onClick={createOrder}
        className="w-full md:h-[50px] md:text-sm bg-[#24b47e] ml-auto hover:bg-[#2f9e44] text-white rounded-md"
      >
        {t('text-submit-order')}
      </Button>
    </div>
  );
}
