import { useMutation } from 'react-query';
import { useTranslation } from 'next-i18next';
import Button from '@/components/ui/button';
import client from '@/data/client';
import { useCart } from '@/components/cart/lib/cart.context';
import toast from 'react-hot-toast';
import { useRouter } from 'next/router';

const PaymentTable = (props: any) => {
  const { items } = useCart();
  const { t } = useTranslation('common');
  const router = useRouter();
  //gửi api sang ví
  const { mutate, isLoading, data } = useMutation(client.payment.post, {
    onSuccess: () => {
      //khi thành công sẽ chuyển đến trang : /orders/id/payment
      //return router.push(`${routes.orderUrl(tracking_number)}/payment`);
      toast.success(<b>{t('text-profile-page-success-toast')}</b>);
    },
    onError: (error: any) => {
      toast.error(<b>{t('text-profile-page-error-toast')}</b>);
    },
  });

  function offTable() {
    props.showPay(false);
  }
  // console.log(props.items);
  function paymentHandler() { }
  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 top-0">
      <div className="relative w-96 h-1/3 mx-auto max-w-screen-sm flex-col p-4 pt-6 sm:p-5 sm:pt-8 md:pt-10 3xl:pt-12 bg-light shadow-card dark:bg-dark-250 dark:shadow-none text-center flex items-center justify-center">
        <button onClick={offTable} className="absolute top-0 right-0 p-4 ">
          X
        </button>

        <h2 className="text-xl flex mb-10">{t('text-payment-title')}</h2>
        <div className="flex">
          <Button className="mr-10" onClick={paymentHandler}>
            {t('text-pay-now')}
          </Button>
          <Button className="bg-red-500 hover:bg-red-300" onClick={offTable}>
            {t('text-cancel')}
          </Button>
        </div>
      </div>
    </div>
  );
};
export default PaymentTable;
