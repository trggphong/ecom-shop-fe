import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import type { GetStaticProps } from 'next';
import { useTranslation } from 'next-i18next';
import type { NextPageWithLayout, UpdateProfileInput } from '@/types';
import { useMutation, useQueryClient } from 'react-query';
import toast from 'react-hot-toast';
import DashboardLayout from '@/layouts/_dashboard';
import client from '@/data/client';
import { useMe } from '@/data/user';
import { API_ENDPOINTS } from '@/data/client/endpoints';
import { useState } from 'react';
import ButtonAction from '@/components/setting/button';
import SettingOTP from '@/components/setting/settingOTP';
import Button from '@/components/ui/button';
import CardOTP from '@/components/setting/cardOTP';

const SettingPage: NextPageWithLayout = () => {
  const { t } = useTranslation('common');
  const { me } = useMe();
  const [action, setAction] = useState('methodOTP');
  function settingAction(input: string) {
    setAction(input);
  }

  return (
    <>
      <h2 className=" text-xl">Cấu hình nhận OTP</h2>
      <ButtonAction action={action} settingAction={settingAction} />
      {action === 'methodOTP' && (
        <SettingOTP me={me} settingAction={settingAction} />
      )}
      {action === 'cardOTP' && <CardOTP />}
    </>
  );
};

SettingPage.authorization = true;
SettingPage.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common'])),
    },
    revalidate: 60, // In seconds
  };
};

export default SettingPage;
