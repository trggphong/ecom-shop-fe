import { useMe } from '@/data/user';
import { useState, useEffect } from 'react';

function useSale() {
  const { me } = useMe();
  const [saleValue, setSaleValue] = useState(0);
  const [salePrice, setSalePrice] = useState(0);
  useEffect(() => {
    if (me && me.package) {
      setSaleValue(me.package.discount_value);
    }
  }, [me]);

  useEffect(() => {
    if (saleValue === 0) {
      setSalePrice(0);
    }
    if (saleValue === 10) {
      setSalePrice(0.9);
    }
    if (saleValue === 20) {
      setSalePrice(0.8);
    }
    if (saleValue === 30) {
      setSalePrice(0.7);
    }
  }, [saleValue]);

  return { salePrice };
}
export default useSale;
